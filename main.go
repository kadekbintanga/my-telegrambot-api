package main

import (
	"my-telegrambot-api/app/helpers"
	"my-telegrambot-api/app/routers"
	"my-telegrambot-api/config"
	"github.com/gin-gonic/gin"
	"os"
)

func main() {
	helpers.LogInfo("==================== MY TELEGRAMBOT API STARTED ====================")
	config.LoadEnv()
	r := gin.Default()
	routers.InitRouter(r)
	r.Run(":"+os.Getenv("PORT"))
}