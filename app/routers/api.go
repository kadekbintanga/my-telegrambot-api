package routers

import (
	"my-telegrambot-api/app/helpers"
	"my-telegrambot-api/app/handlers"
	"github.com/gin-gonic/gin"
)

func InitRouter(r *gin.Engine){
	HealthHandler := handlers.NewHealthHandler()
	SendMessageHandler := handlers.NewSendMessageHandler()
	api := r.Group("/mytelegrambot/api")
	apiV1 := api.Group("/v1")
	apiV1.GET("/health", HealthHandler.HealthCheck)
	apiV1.POST("/send/message/bintang-project-bot", SendMessageHandler.SendMessageToBintangProjectBot)

	helpers.LogInfo("========== Server Started ==========")
}