package handlers

import (
	"my-telegrambot-api/app/resources"
	"my-telegrambot-api/app/services"
	"my-telegrambot-api/app/helpers"
	"github.com/gin-gonic/gin"
	"encoding/json"
	"io/ioutil"
)

type SendMessageHandler struct {
	service *services.SendMessageService

}

func NewSendMessageHandler() *SendMessageHandler{
	return &SendMessageHandler{
		service: services.NewSendMessageService(),
	}
}

func(h *SendMessageHandler) SendMessageToBintangProjectBot(c *gin.Context) {
	helpers.LogInfo("==================== SEND MESSAGE TO BINTANG PROJECT BOT START ====================")
	defer helpers.LogInfo("==================== SEND MESSAGE TO BINTANG PROJECT BOT END ====================")
	bodyData,_ := ioutil.ReadAll(c.Request.Body)
	string_data_request := string(bodyData)
	helpers.LogInfo("Data Request : "+ string_data_request)
	
	var req resources.RequestSendMessage
	err := json.Unmarshal(bodyData, &req)
	if err != nil {
		helpers.LogError(err.Error())
		message := helpers.RES_400_INVALID_REQUEST
		response := helpers.JsonResponseGeneral(message.Code, message.Status, message.Message ,nil, nil)
		c.JSON(message.Code, response)
		return
	}

	process_request := h.service.SendMessageToBintangProjectBot(req.Message)
	response := helpers.JsonResponseGeneral(process_request.Code, process_request.Status, process_request.Message ,nil, nil)
	c.JSON(process_request.Code, response)
	return
}