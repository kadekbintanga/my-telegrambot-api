package handlers

import (
	"github.com/gin-gonic/gin"
	"my-telegrambot-api/app/helpers"
)

type HealthHandler struct {
}

func NewHealthHandler() *HealthHandler{
	return &HealthHandler{}
}

func(h *HealthHandler) HealthCheck(c *gin.Context){
	helpers.LogInfo("SMS Health is hited")
	message := helpers.RES_200_HEALTH
	response := helpers.JsonResponseGeneral(message.Code, message.Status, message.Message ,nil, nil)
	c.JSON(message.Code, response)
	return
}
