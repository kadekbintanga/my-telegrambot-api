package services


import (
	"my-telegrambot-api/app/integrations"
	"my-telegrambot-api/app/resources"
	"my-telegrambot-api/app/helpers"
	"os"
)

type SendMessageService struct {
	itgt_telebot integrations.TelegramBotIntegrations

}

func NewSendMessageService() *SendMessageService{
	return &SendMessageService{
		itgt_telebot: integrations.NewTelegramBotIntegrations(),
	}
}


func(s *SendMessageService) SendMessageToBintangProjectBot(message string)(helpers.MappingResponseCode){
	if message == "" {
		return helpers.RES_400_EMPTY_MESSAGE
	}
	url := os.Getenv("TELEGRAM_BOT_BASE_URL")+os.Getenv("TELEGRAM_TOKEN_BINTANGPROJECT_BOT")+os.Getenv("TELEGRAM_BOT_SEND_MESSAGE")
	payload := resources.RequestTelegramBotSendMessage{
		ChatID: os.Getenv("TELEGRAM_CHATID_BINTANG"),
		Text: message,
	}
	itgt_telebot := s.itgt_telebot
	go itgt_telebot.SendMessage(payload, url)
	return helpers.RES_200_PROCESSED
}