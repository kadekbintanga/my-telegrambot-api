package resources

type RequestTelegramBotSendMessage struct {
	ChatID string  	`json:"chat_id"`
	Text   string 	`json:"text"`
}