package helpers

 
import (
	"log"
)

 

func PrintLog(tag string, message any) {
    log.Println("[" + tag + "]", message)
}

func LogDebug(message any) {
    PrintLog("DEBUG", message)
}

func LogInfo(message any) {
    PrintLog("INFO", message)
}

func LogWarning(message any) {
    PrintLog("WARNING", message)
}

func LogError(message any) {
    PrintLog("ERROR", message)
}