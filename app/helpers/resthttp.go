package helpers

import (
	"bytes"
	"io/ioutil"
	"net/http"
	"fmt"
)

type HttpRestClient struct {
	Url      	string
	Header   	map[string]string
	Payload  	[]byte
	Params		map[string]string
	Body     	string
}

func NewHttpRestClient() HttpRestClient {
	return HttpRestClient{
		Header: map[string]string{
			"Content-Type": "application/json",
		},
		Params: map[string]string{},
	}
}

func (s *HttpRestClient) Post(out *[]byte, status_code *int)error{
	client := &http.Client{}
	request, err := http.NewRequest("POST", s.Url, bytes.NewBuffer(s.Payload))
	if err != nil {
		LogError("Error new request : "+err.Error())
		*status_code = 500
		*out = []byte(err.Error())
		return err
	}
	for key, val := range s.Header {
		request.Header.Add(key, val)
	}
	LogInfo("[POST] ==> " + s.Url)
	resp, err := client.Do(request)
	if err != nil {
		LogError("Error request : "+ err.Error())
		*status_code = 500
		*out = []byte(err.Error())
		return err
	}
	defer resp.Body.Close()
	LogInfo("==== [RESPONSE] ====")
	LogInfo("[STATUS CODE] ==> "+ fmt.Sprint(resp.StatusCode))
	*status_code = resp.StatusCode
	*out, err = ioutil.ReadAll(resp.Body)
	if err != nil {
		LogError("Error request : "+ err.Error())
		*status_code = 500
		*out = []byte(err.Error())
		return err
	}
	LogInfo("[RESPONSE BODY] ==> "+ string(*out))
	return nil
}

func (s *HttpRestClient) Get(out *[]byte, status_code *int)error{
	client := &http.Client{}
	request, err := http.NewRequest("GET", s.Url, nil)
	if err != nil {
		LogError("Error new request : "+ err.Error())
		*status_code = 500
		*out = []byte(err.Error())
		return err
	}
	for key, val := range s.Header {
		request.Header.Add(key, val)
	}
	if len(s.Params) > 0 {
		params := request.URL.Query()
		for key, val := range s.Params {
			params.Add(key, val)
		}
		request.URL.RawQuery = params.Encode()
	}
	LogInfo("[GET] ==> " + s.Url)
	resp, err := client.Do(request)
	if err != nil {
		LogError("Error request : "+ err.Error())
		*status_code = 500
		*out = []byte(err.Error())
		return err
	}
	defer resp.Body.Close()
	LogInfo("==== [RESPONSE] ====")
	LogInfo("[STATUS CODE] ==> "+ fmt.Sprint(resp.StatusCode))
	*status_code = resp.StatusCode
	*out, err = ioutil.ReadAll(resp.Body)
	if err != nil {
		LogError("Error read data response body : "+ err.Error())
		*status_code = 500
		*out = []byte(err.Error())
		return err
	}
	LogInfo("[RESPONSE BODY] ==> "+ string(*out))
	return nil
}
