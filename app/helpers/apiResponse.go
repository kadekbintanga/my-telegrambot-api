package helpers

type ResponseGeneral struct {
	Code			int				`json:"code"`
	Status			string			`json:"status"`
	Message			string			`json:"message"`
	Data			interface{}		`json:"data"`
	Meta			interface{}		`json:"meta"`
}

func JsonResponseGeneral(code int, status, message string,additionalData ...interface{}) ResponseGeneral{
	var responseData interface{}
	if len(additionalData) > 0 {
		responseData = additionalData[0]
	} else {
		responseData = map[string]interface{}{}
	}
	var responseMeta interface{}
	if len(additionalData) > 1 {
		responseMeta = additionalData[1]
	} else {
		responseMeta = map[string]interface{}{}
	}
	res := ResponseGeneral{
		Code: code,
		Status: status,
		Message: message,
		Data: responseData,
		Meta: responseMeta,
	}
	return res
}