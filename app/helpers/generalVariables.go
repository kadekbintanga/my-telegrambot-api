package helpers

import (
	"net/http"
)

type MappingResponseCode struct{
	Code		int
	Status		string
	Message		string
}


// HTTP RESPONSE 200
var RES_200_HEALTH = MappingResponseCode{Code:http.StatusOK, Status:"Success", Message:"I am ready...!"}
var RES_200_PROCESSED = MappingResponseCode{Code:http.StatusOK, Status:"Success", Message:"Data success processed"}

// HTTP RESPONSE 400
var RES_400_INVALID_REQUEST = MappingResponseCode{Code:http.StatusBadRequest, Status:"Bad request", Message:"Invalid data request, please check your data request"}
var RES_400_EMPTY_MESSAGE = MappingResponseCode{Code:http.StatusBadRequest, Status:"Bad request", Message:"Message value is missing"}

// HTTP RESPONSE 500
var RES_500_ERROR = MappingResponseCode{Code:http.StatusInternalServerError, Status:"Internal Server Error", Message:"Something went wrong, please try again latter"}