package integrations

import (
	"my-telegrambot-api/app/helpers"
	"my-telegrambot-api/app/resources"
	"encoding/json"
	"fmt"
)


type TelegramBotIntegrations interface {
	SendMessage(payload resources.RequestTelegramBotSendMessage, url string)
}

func NewTelegramBotIntegrations() TelegramBotIntegrations {
	return &integration{
		request: helpers.NewHttpRestClient(),
	}
}

func(i *integration) SendMessage(payload resources.RequestTelegramBotSendMessage, url string){
	defer i.catchErrTeleBintangProBotIntergration()
	data_request,_ := json.Marshal(payload)
	helpers.LogInfo("[Data Callback to Merchant] ===> " + string(data_request))
	var response []byte
	var status_code int
	request := i.request
	request.Url = url
	request.Header["Content-Type"] = "application/json"
	request.Payload = data_request
	err := request.Post(&response, &status_code)
	if err != nil {
		helpers.LogError("Hit to Merchant Callback : "+ err.Error())
	}
	return

}

func(i *integration) catchErrTeleBintangProBotIntergration(){
	if r := recover(); r != nil {
        if errStr, ok := r.(string); ok {
            helpers.LogError("Error occurred in Merchant integration: " + errStr)
        } else {
            helpers.LogError("Error occurred in Merchant integration: " + fmt.Sprint(r))
        }
    }
}